#define NODEMCU_LED D4

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(NODEMCU_LED, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(NODEMCU_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(2000);                       // wait for a second
  digitalWrite(NODEMCU_LED, LOW);    // turn the LED off by making the voltage LOW
  delay(2000);                       // wait for a second
}
