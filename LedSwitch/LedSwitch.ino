#include <ESP8266WiFi.h>

#define LED D4

const char* ssid = "HOME";
const char* password = "agora_pensa";

int ledValue = HIGH;

WiFiServer server(80);

char* getLEDState(int value) {
  switch(value) {
    case LOW:
      return "on";
    case HIGH:
      return "off";
    default:
      return "?";
  }
}

void setup() {
  Serial.begin(115200);

  // Setup LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED, ledValue);

  Serial.println();
  Serial.println();

  // Connect to WiFi network
  WiFi.begin(ssid, password);

  Serial.printf("Connecting to %s ", ssid);
  Serial.print(ssid);
  Serial.print(" ");

  int retries = 20;
  while (WiFi.status() != WL_CONNECTED && retries > 0) {
    delay(500); //Wait 500ms and try again
    retries--;
    Serial.print(".");
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println(" DONE!");
  } else {
    Serial.println(" FAIL!");
    return;
  }

  // Start the server
  server.begin();
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}
 
void loop() {
  // Listen for incoming clients
  WiFiClient client = server.available();
  
  if (!client) {
    // No client found
    return;
  }
  
  // Wait until the client sends some data
  while (!client.available()) {
    delay(500);
  }
  
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  client.flush();

  Serial.println();
  Serial.println("New request!");
  Serial.println(request);
  
  // Match the request
  if (request.indexOf("/LED=ON") != -1) {
    ledValue = LOW;
  }
  
  if (request.indexOf("/LED=OFF") != -1) {
    ledValue = HIGH;
  }
 
  // Set ledPin according to the request
  digitalWrite(LED, ledValue);
 
  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); // Do not forget this one
  
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.printf("<p>LED state: %s </p>", getLEDState(ledValue));
  client.printf("<a href=\"%s\"><button>%s</button></a> ", "/LED=ON", "Turn On");
  client.printf("<a href=\"%s\"><button>%s</button></a> ", "/LED=OFF", "Turn Off");  
  client.println("</html>");
}
